<?php

namespace app\models;

use yii\db\ActiveRecord;

class Student extends ActiveRecord
{
    public static function tableName(){
		return 'student';
	}
	
	public static function getOneStudent(){
			$student = self::find()->one();
			return $student;
			
		}

	public static function getStudents(){
		$student = self::find()->all();
		
		return $student;
	
	}
			
}
	