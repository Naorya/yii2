<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movies".
 *
 * @property integer $id
 * @property string $Movie_Name
 * @property string $Gener
 * @property string $Min_Age
 * @property string $Movie_Grade
 */
class Movies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Movie_Name', 'Gener', 'Min_Age', 'Movie_Grade'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Movie_Name' => 'Movie  Name',
            'Gener' => 'Gener',
            'Min_Age' => 'Min  Age',
            'Movie_Grade' => 'Movie  Grade',
        ];
    }
}
