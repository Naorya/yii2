<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View Student';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php foreach($student as $value): ?>
    <p>
		<b>Name:</b> <?= $value->name ?> </br>
		<b>ID:</b> <?= $value->id ?> </br>
		<b>Age:</b> <?= $value->age ?> </br>
    </p>
<?php endforeach; ?>
    <code><?= __FILE__ ?></code>
</div>