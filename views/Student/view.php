<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View Student';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

   <p>
		<b>Name:</b> <?= $student->name ?> </br>
		<b>ID:</b> <?= $student->id ?> </br>
		<b>Age:</b> <?= $student->age ?> </br>
    </p>

    <code><?= __FILE__ ?></code>
</div>