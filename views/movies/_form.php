<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Movie_Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Gener')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Min_Age')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Movie_Grade')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
