<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movies`.
 */
class m170612_090605_create_movies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movies', [
            'id' => $this->primaryKey(),
            'Movie_Name' => $this->string(),
            'Gener' => $this->string(),
            'Min_Age' => $this->string(),
            'Movie_Grade'=> $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movies');
    }
}
